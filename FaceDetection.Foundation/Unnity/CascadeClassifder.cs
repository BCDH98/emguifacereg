﻿
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaceDetection.Foundation.Unnity
{
    public class CascadeClassifder
    {
        private readonly CascadeClassifier cascade;
        private CascadeClassifier face;
        private CascadeClassifier eyes;
        private Image<Gray, byte> result, TrainedFace = null;
        private Image<Gray, byte> gray = null;
        private List<Image<Gray, byte>> trainingImages = new List<Image<Gray, byte>>();
        private List<string> labels = new List<string>();
        private List<string> NamePersons = new List<string>();
        private int ContTrain, NumLabels, t;
        private string name, names = null;
        private Image<Bgr, byte> currentFrame;
        private Capture grabber;
        private bool _isLoaded { get; set; }
        public CascadeClassifder()
        {

            cascade = new CascadeClassifier(FilePath("\\Content\\CascadeFile\\haarcascade_frontalface_default.xml"));
            
        }

        public string FilePath(string path)
        {
            string filePath = Application.StartupPath + path;
            return filePath;
        }
        public void OpenImageDetect(ImageBox img)
        {
            try
            {
                using (OpenFileDialog fileDialog = new OpenFileDialog() { Multiselect = false, Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*" })
                {
                    if (fileDialog.ShowDialog() == DialogResult.OK)
                    {
                        Image<Bgr, byte> bgrImage = new Image<Bgr, byte>(fileDialog.FileName);
                        img.Image = bgrImage;
                        Bitmap bitmap = bgrImage.ToBitmap();
                        Image<Gray, byte> grayImage = new Image<Gray, byte>(fileDialog.FileName);
                        Rectangle[] rectangles = cascade.DetectMultiScale(grayImage, 1.1, 2, new Size(25, 25));
                        foreach (var item in rectangles)
                        {
                            //using (Graphics graphics = Graphics.FromImage(bitmap))
                            //{
                            //    using (Pen pen = new Pen(Color.Red, 1))
                            //    {
                            //        graphics.DrawRectangle(pen, item);
                            //    }
                            //}
                            bgrImage.Draw(item, new Bgr(Color.Green), 2);
                        }
                        //img.Image = bitmap.ToImage<Bgr, byte>();
                        img.Image = bgrImage;

                    }

                }
            }
            catch (Exception)
            {


                MessageBox.Show("Wrong Fromat!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }

        }

        public bool Is_LoadCascade()
        {
            face = new CascadeClassifier(FilePath("\\Content\\CascadeFile\\haarcascade_frontalface_default.xml"));
            eyes = new CascadeClassifier(FilePath("\\Content\\CascadeFile\\haarcascade_eye.xml"));
            try
            {
                string Labelsinfo = File.ReadAllText(FilePath("\\Content\\TrainedFaces\\TrainedLabels.txt"));
                string[] Labels = Labelsinfo.Split('%');
                NumLabels = Convert.ToInt16(Labels[0]);
                ContTrain = NumLabels;
                string LoadFaces;
                for (int tf = 1; tf < NumLabels + 1; tf++)
                {
                    LoadFaces = "face" + tf + ".bmp";
                    trainingImages.Add(new Image<Gray,byte>(FilePath("\\Content\\TrainedFaces\\") + LoadFaces));
                    labels.Add(Labels[tf]);
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Nothing in binary database, please add at least a face", "Triained faces load", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return true;

        }

    }
}


