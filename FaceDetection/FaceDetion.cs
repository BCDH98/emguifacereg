﻿using FaceDetection.Foundation.Unnity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaceDetection
{
    public partial class FaceDetion : Form
    {
        
        private readonly CascadeClassifder cas;
        public FaceDetion()
        {
            InitializeComponent();
            cas = new CascadeClassifder();
            cas.Is_LoadCascade();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {

            cas.OpenImageDetect(ib1);
        }
    }
}
